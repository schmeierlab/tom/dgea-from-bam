#!/usr/bin/Rscript
#
# MSigDB-term enrichment analysis (NOT TRADITIONAL GSEA)
#
# Reads table from standard in
# expects first column to be ID to be 
# be converted to Entrez Gene IDs, thus
# not all genes may be part of the analysis
#
# The UNIVERSE for enrichment analysis are taken as all genes
# in the gmt file. So it is advisable to not submit only a few
# term list with a limited number of genes
#
time <- format(Sys.time(), "%Y%m%d-%H%M%S")
library(clusterProfiler)
library(readr)

args <- commandArgs(trailingOnly = TRUE)
genecol <- as.character(args[1])
genefc  <- as.numeric(args[2])
genefdr <- as.numeric(args[3])
genemax <- as.numeric(args[4])  # maximum number of genes as input to enrichement analysis

fdr <- as.numeric(args[5])
gmtfile <- args[6]
idtype  <- args[7]  # ENTREZID, SYMBOL, ENSEMBL
organism <-  args[8]

outfileUP <- args[9]
outplotUP <- args[10]
outfileDN <- args[11]
outplotDN <- args[12]

# data from stdin ========================================
targets <- read.table(file("stdin"), header = TRUE)

# remove the ones not tested
targets <- targets[with(targets, !is.na(targets$FDR)), ]
de <- targets[targets$FDR<genefdr, ]

# extract the ones with desired fc
up <- de[de$logFC>=genefc, ]
dn <- de[de$logFC<=genefc, ]

# rank genes based on -log10(pV) * log2fc
## https://www.pathwaycommons.org/guide/primers/data_analysis/gsea/

up$rank <- -log10(up$PValue) * sign(up$logFC)
dn$rank <- -log10(dn$PValue) * sign(dn$logFC)

# sort after rank
up <- up[with(up, order(-up$rank)), c(genecol)]
dn <- dn[with(dn, order(dn$rank)), c(genecol)]

# ========================================================
if(organism=='HSA'){
    library(org.Hs.eg.db)
    orgdb = "org.Hs.eg.db"
} else if(organism=='MMU'){
    library(org.Mm.eg.db)
    orgdb = "org.Mm.eg.db"
} else {
    stop()
}

# now convert IDs
if (idtype == "ENTREZID") {
    up <- as.data.frame(up)
    dn <- as.data.frame(dn)
    colnames(up) <- c("ENTREZID")
    colnames(dn) <- c("ENTREZID")
} else {
    # translate into entrez gene id, we will loose some
    #targetsEG <- bitr(targets[,1], fromType="ENSEMBL", toType="ENTREZID", OrgDb="org.Hs.eg.db")
    # idtype e.g.  ENSEMBL, SYMBOL, MGI
    up <- bitr(up, fromType=idtype, toType="ENTREZID", OrgDb=orgdb)
    dn <- bitr(dn, fromType=idtype, toType="ENTREZID", OrgDb=orgdb)
}

# subselect top x
up <- head(up, n=genemax)
dn <- head(dn, n=genemax)

gmt <- read.gmt(gmtfile)
# the gene universe is assumed to be all genes by default
# create universe of all entrez gene ids with a term associated
universe <- unique(gmt$gene)

# UP
egmt <- enricher(gene = up$ENTREZID,
     		     pAdjustMethod = "BH",
		         pvalueCutoff = fdr,
		         TERM2GENE=gmt,
		         universe=universe)

pdf(outplotUP, width=20, height=14)
dotplot(egmt, showCategory=30, font.size=16)
dev.off()

gz <- gzfile(outfileUP, "w")
write.table(egmt,
            file=gz,
            append=FALSE,
            quote=FALSE,
            sep="\t",
            row.names=FALSE)
close(gz)


# DN
egmt <- enricher(gene = dn$ENTREZID,
     		     pAdjustMethod = "BH",
		         pvalueCutoff = fdr,
		         TERM2GENE=gmt,
		         universe=universe)

pdf(outplotDN, width=20, height=14)
dotplot(egmt, showCategory=30, font.size=16)
dev.off()

gz <- gzfile(outfileDN, "w")
write.table(egmt,
            file=gz,
            append=FALSE,
            quote=FALSE,
            sep="\t",
            row.names=FALSE)
close(gz)
