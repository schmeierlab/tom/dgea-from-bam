import sys, os, glob

p = sys.argv[1]

files = glob.glob("{}/*.bam".format(p))
files.sort()

print("sample\tunit\tbam")

for f in files:
    sample = f.split("/")[-1].split(".")[0]
    print(f"{sample}\t1\t{f}")
