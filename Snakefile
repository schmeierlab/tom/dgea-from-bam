## =============================================================================
## WORKFLOW PROJECT: dgea-from-bam
## INIT DATE: 2020
import pandas as pd
import glob, os, datetime, sys, csv, logging
from os.path import join, abspath, isfile
from snakemake.utils import validate, min_version

## ============================================================================
## require minimum snakemake version ##
min_version("5.16.0")

## ============================================================================
## SETUP
## ============================================================================

## SET SOME DEFAULT PATH
DIR_SCRIPTS = abspath("scripts")
DIR_WRAPPER = abspath("wrapper")
DIR_REPORT  = abspath("report")
DIR_ENVS    = abspath("envs")
DIR_RULES   = abspath("rules")
DIR_SCHEMAS = abspath("schemas")

## LOAD VARIABLES FROM CONFIGFILE
## submit on command-line via --configfile
if config=={}:
    sys.stderr.write('Please submit config-file with "--configfile <file>". Exit.\n')
    sys.exit(1)

sys.stderr.write("********** Submitted config: **********\n")
for k,v in config.items():
    sys.stderr.write("{}: {}\n".format(k,v))
sys.stderr.write("***************************************\n")

## Validate configfile with yaml-schema
# validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))

## define global Singularity image for reproducibility
## USE: "--use-singularity" to run all jobs in container
singularity: config["singularity"] 

## Setup result dirs
DIR_BASE       = abspath(config["resultdir"])
DIR_LOGS       = join(DIR_BASE, "logs")
DIR_BENCHMARKS = join(DIR_BASE, "benchmarks")
DIR_RES        = join(DIR_BASE, "results")

## Workflow specific setup from config ========================================

fc_extra = config["extra"]["featurecounts"]
if config["paired_end"]:
    fc_extra += " -p -B -C"

# check strandedness parameter if we map
if config["strandedness"] not in ["None", "yes", "reverse"]:
    sys.stderr.write('Unknown strandedness values in config. Unstranded libraries assumed.\n')
if config["strandedness"] == "reverse":
    # for featureCount
    fc_stranded = "-s 2"
elif config["strandedness"] == "yes":
    fc_stranded = "-s 1"
else:
    fc_stranded = "-s 0"

# DGEA TOOL SETUP 
TOOL = config["diffexp"]["tool"]

# FORCE deseq2 for paired analysis, edgeR not yet working
if config["diffexp"]["paired"] == True:
    TOOL = "deseq2"

if TOOL not in ["deseq2", "edgeR"]:
    sys.stderr.write("The workflow for tool {} is unknown in this context. EXIT".format(TOOL))
    sys.exit()
if TOOL == "deseq2":
    GSEA_SCRIPT = join(DIR_SCRIPTS, "gsea_msigdb.R")
    FEA_SCRIPT = join(DIR_SCRIPTS, "enrichment_msigdb.R")
elif TOOL == "edgeR":
    GSEA_SCRIPT = join(DIR_SCRIPTS, "gsea_msigdb_edgeR.R")
    FEA_SCRIPT = join(DIR_SCRIPTS, "enrichment_msigdb_edgeR.R")
    

## ============================================================================
## Reading samples from sheet
samples = pd.read_csv(config["samples"], sep="\t").set_index("sample", drop=False)
# validate samplesheet
# validate(samples, schema=join(DIR_SCHEMAS, "samples.schema.yaml"))

units = pd.read_table(config["units"], dtype=str).set_index(["sample", "unit"], drop=False)
units.index = units.index.set_levels([i.astype(str) for i in units.index.levels])  # enforce str in index
# validate(units, schema=join(DIR_SCHEMAS, "units.schema.yaml"))

## ============================================================================
## FUNCTIONS
## ============================================================================
def get_bam(wildcards):
    return units.loc[(wildcards.sample, wildcards.unit), ["bam"]].dropna()


def get_fcounts_files(wildcards):
    files = []
    for u in units.loc[wildcards.sample, ["unit"]]["unit"].tolist():
        files.append(join(DIR_RES, "01_fc/{}-{}.tsv".format(wildcards.sample, u)))
    return files

## ============================================================================
## SETUP FINAL TARGETS
## ============================================================================
TARGETS = [join(DIR_RES, "02_expression/all.tpm.tsv"), 
           join(DIR_RES, "02_expression/all.counts.tsv")]

if config["diffexp"]["do"] == True:
    CONTRASTS = config["diffexp"]["contrasts"]
    # add diffexp result files too
    TARGETS += [expand([join(DIR_RES, "03_diffexp/{contrast}/diffexp.tsv.gz"),
                    join(DIR_RES, "03_diffexp/{contrast}/ma-plot.pdf")],
                contrast=CONTRASTS)]

    TARGETS += [join(DIR_RES, "pca.pdf")]

    # if config["gsea"]["do"]:
        # TARGETS += [expand(join(DIR_RES, "04_fea/{contrast}.diffexp.fea-up.tsv.gz"), contrast=CONTRASTS),
        #             expand(join(DIR_RES, "05_gsea/{contrast}.diffexp.gsea-up.tsv.gz"), contrast=CONTRASTS),
        #             expand(join(DIR_RES, "05_gsea/{contrast}.diffexp.gsea-up.selected.tsv.gz"), contrast=CONTRASTS)]


## ============================================================================
## RULES
## ============================================================================

## Pseudo-rule to state the final targets, so that the whole runs
rule all:
    input:
        TARGETS


rule featurecounts:
    input:
        get_bam
    output:
        join(DIR_RES, "01_fc/{sample}-{unit}.tsv")
    conda:
        join(DIR_ENVS, "subread.yaml")
    log:
        join(DIR_LOGS, "featurecounts/{sample}-{unit}.log")
    benchmark:
        join(DIR_BENCHMARKS, "featurecounts/{sample}-{unit}.txt")
    threads: 8
    priority: 30
    params:
        anno=config["annotation"],
        stranded=fc_stranded,
        extra=fc_extra
    shell:
        "featureCounts {params.stranded} {params.extra} "
        "-T {threads} "
        "-a {params.anno} "
        "-t exon "
        "-g gene_id "
        "-o {output[0]} {input} > {log} 2>&1"


rule sum_unit_counts:
    input:
        get_fcounts_files
    output:
        counts=temp(join(DIR_RES, "02_expression/{sample}.counts.txt")),
        tpm=temp(join(DIR_RES, "02_expression/{sample}.tpm.txt"))
    conda:
        join(DIR_ENVS, "pandas.yaml")
    log:
        join(DIR_LOGS, "sum_unit_counts/{sample}.log")
    benchmark:
        join(DIR_BENCHMARKS, "sum_unit_counts/{sample}.txt")
    params:
        script=join(DIR_SCRIPTS, "combine_fcounts.py"),
        sample="{sample}"
    shell:
        "python {params.script} "
        "--counts {output.counts} "
        " --tpm {output.tpm} "
        "{params.sample} {input} "
        "> {log} 2>&1"

    
rule get_expression_count_table:
    input:
        expand(join(DIR_RES, "02_expression/{sample}.counts.txt"), 
                sample=samples["sample"])
    output:
        join(DIR_RES, "02_expression/all.counts.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "get_expression_count_table/all_counts.txt")
    conda:
        join(DIR_ENVS, "pandas.yaml")
    script:
        join(DIR_SCRIPTS, "count_matrix.py")


rule get_expression_tpm_table:
    input:
        expand(join(DIR_RES, "02_expression/{sample}.tpm.txt"), 
                sample=samples["sample"])
    output:
        join(DIR_RES, "02_expression/all.tpm.tsv")
    benchmark:
        join(DIR_BENCHMARKS, "get_expression_tpm_table/all_tpm.txt")
    conda:
        join(DIR_ENVS, "pandas.yaml")
    script:
        join(DIR_SCRIPTS, "count_matrix.py")


# rule prepare_file_for_gsea:
#     input:
#         join(DIR_RES, "host/contrasts/{contrast}/diffexp.tsv.gz")
#     output:
#         temp(join(DIR_RES, "host/contrasts/{contrast}/diffexp.rnk"))
#     benchmark:
#         join(DIR_BENCHMARKS, "host/contrasts/{contrast}_gsea_prep.txt")
#     conda:
#         join(DIR_ENVS, "pandas.yaml")
#     script:
#         join(DIR_SCRIPTS, "prepare_ranks.py")


# rule gsea_gmt:
#     input:
#         join(DIR_RES, "host/contrasts/{contrast}/diffexp.rnk")
#     output:
#         join(DIR_RES, "host/contrasts/{contrast}/diffexp.gsea-up.tsv.gz"),
#         join(DIR_RES, "host/contrasts/{contrast}/diffexp.gsea-dn.tsv.gz")
#     log:
#         join(DIR_LOGS, "host/contrasts/{contrast}_gsea.log")
#     benchmark:
#         join(DIR_BENCHMARKS, "host/contrasts/{contrast}_gsea.txt")
#     conda:
#         join(DIR_ENVS, "clusterprofiler.yaml")
#     singularity:
#         "shub://sschmeier/container-gsea"
#     params:
#         script=join(DIR_SCRIPTS, "gsea_msigdb_edgeR.R"),
#         gmt=config["gsea_gmt"],
#         nperms=10000
#     shell:
#         "cat {input} | Rscript --vanilla --slave {params.script} "
#         "{params.gmt} {params.nperms} {output[0]} {output[1]} > {log} 2>&1"

# run with 
# snakemake --configfile config.yaml clean
rule clean:
    shell:
        "rm -rf {DIR_BASE}/*"


include: join(DIR_RULES, "diffexp-{}.smk".format(TOOL))
