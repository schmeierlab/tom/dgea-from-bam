def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]


rule pca2:
    input:
        counts=join(DIR_RES, "02_expression/all.counts.tsv")
    output:
        join(DIR_RES, "pca.pdf")
    params:
        pca_labels=config["pca"]["labels"],
        samples=config["samples"],
        paired=config["diffexp"]["paired"],
        column=config["diffexp"]["column"]
    conda:
        join(DIR_ENVS, "deseq2.yaml")
    log:
        join(DIR_LOGS, "pca.log")
    benchmark:
        join(DIR_BENCHMARKS, "pca2.txt")
    script:
        join(DIR_SCRIPTS, "plot-pca2.R")


rule edgeR:
    input:
        counts=join(DIR_RES, "02_expression/all.counts.tsv")
    output:
        table=join(DIR_RES, "03_diffexp/{contrast}/diffexp.tsv.gz"),
        ma_plot=join(DIR_RES, "03_diffexp/{contrast}/ma-plot.pdf")
    params:
        contrast=get_contrast,
        samples=config["samples"],
        paired=config["diffexp"]["paired"],
        column=config["diffexp"]["column"],
        mincount=config["diffexp"]["mincount"]
    conda:
        join(DIR_ENVS, "edgeR.yaml")
    log:
        join(DIR_LOGS, "edgeR/{contrast}.diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "edgeR/{contrast}.diffexp.txt")
    script:
        join(DIR_SCRIPTS, "edgeR.R")

