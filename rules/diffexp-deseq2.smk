def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]


rule deseq2_init:
    input:
        counts=join(DIR_RES, "02_expression/all.counts.tsv")
    output:
        join(DIR_RES, "03_diffexp/deseq2/dds.rds")
    params:
        samples=config["samples"],
        paired=config["diffexp"]["paired"],
        column=config["diffexp"]["column"]
    conda:
        join(DIR_ENVS, "deseq2.yaml")
    log:
        join(DIR_LOGS, "deseq2/init.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2_init.txt")
    threads: 8
    script:
        join(DIR_SCRIPTS, "deseq2-init.R")


rule pca:
    input:
        join(DIR_RES, "03_diffexp/deseq2/dds.rds")
    output:
        join(DIR_RES, "pca.pdf"),
        join(DIR_RES, "counts-normalized.txt.gz")
    params:
        pca_labels=config["pca"]["labels"]
    conda:
        join(DIR_ENVS, "deseq2.yaml")
    log:
        join(DIR_LOGS, "pca.log")
    benchmark:
        join(DIR_BENCHMARKS, "pca.txt")
    script:
        join(DIR_SCRIPTS, "plot-pca.R")


rule deseq2:
    input:
        join(DIR_RES, "03_diffexp/deseq2/dds.rds")
    output:
        table=join(DIR_RES, "03_diffexp/{contrast}/diffexp.tsv.gz"),
        ma_plot=join(DIR_RES, "03_diffexp/{contrast}/ma-plot.pdf")
    params:
        contrast=get_contrast,
        filterFun="ihw"
    conda:
        join(DIR_ENVS, "deseq2.yaml")
    log:
        join(DIR_LOGS,"deseq2/{contrast}.diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2/{contrast}.diffexp.txt")
    threads: 8
    script:
        join(DIR_SCRIPTS, "deseq2.R")
